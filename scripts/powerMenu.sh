#!/bin/bash

### Options ###
power_off="Shutdown"
reboot="Reboot"
suspend="Suspend"

# Variable passed to rofi #
options="$power_off\n$reboot\n$suspend"

selection=$(echo -e "Shutdown\nReboot\nSuspend" | rofi -dmenu -i -p "power" -lines 3)

echo "$selection"

case $selection in
    $power_off)
        shutdown -h now
        ;;
    $reboot)
        shutdown -r now
        ;;
    $suspend)
        systemctl suspend
        ;;
esac